steal('jquery/class', function () {

    $.Class('SimpleCqrs.Framework.Events.Event',
    /* @Static */
        {

    },
    /* @Prototype */
        {
        setup: function () {
            this.eventName = this.Class.fullName;
        },

        setVersion: function (version) {
            this.version = version;
            this.eventId = this.id + "-" + version;
            return this;
        }
    });

})