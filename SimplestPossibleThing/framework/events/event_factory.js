steal('jquery/class', function () {

    $.Class('SimpleCqrs.Framework.Events.EventFactory',
    /* @Static */
        {
        createEvent: function (name, expectedAttributes) {

            // Define the init function for this particular set of attributes:
            var initialiser = function (attrs) {
                // Ensure that there is a value in the attrs object for each expectedAttribute
                for (var i = 0; i < expectedAttributes.length; i++) {
                    if (attrs[expectedAttributes[i]] === undefined) {
                        throw "InvalidOperation";
                    }
                }

                $.extend(this, attrs);
            }

            // Create the event
            SimpleCqrs.Framework.Events.Event(name, {}, { init: initialiser });
        }
    },
    /* @Prototype */
        {
    });

})