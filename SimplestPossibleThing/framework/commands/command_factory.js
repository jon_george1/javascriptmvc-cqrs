steal('jquery/class', function () {

    $.Class('SimpleCqrs.Framework.Commands.CommandFactory',
    /* @Static */
        {
        createCommand: function (name, expectedAttributes) {

            // Define the init function for this particular set of attributes:
            var initialiser = function (attrs) {
                // Ensure that there is a value in the attrs object for each expectedAttribute
                for (var i = 0; i < expectedAttributes.length; i++) {
                    if (attrs[expectedAttributes[i]] === undefined) {
                        throw "InvalidOperation";
                    }
                }

                $.extend(this, attrs);
            }

            // Create the event
            $.Class(name, {}, { init: initialiser });
        }
    },
    /* @Prototype */
        {
    });

})