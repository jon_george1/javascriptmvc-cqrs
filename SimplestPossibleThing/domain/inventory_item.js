steal(
    './aggregate_root',
    './events',
    function () {

        SimpleCqrs.Domain.AggregateRoot('SimpleCqrs.Domain.InventoryItem',
        /* @Static */
        {

    },
    /* @Prototype */
        {
        activated: true,

        id: null,

        init: function (id, name) {
            this.changes = [];

            // If no id/name is specified, assume the class is being recreated from events. Otherwise, assume
            // it is being created as a new instance.
            if (id !== undefined && name !== undefined && name.length !== 0) {
                this.applyChange(new SimpleCqrs.Domain.Events.InventoryItemCreated({ id: id, name: name }));
            }
        },

        apply: function (event) {
            switch (event.eventName) {
                case "SimpleCqrs.Domain.Events.InventoryItemCreated":
                    this.id = event.id;
                    this.name = event.name;
                    break;
                case "SimpleCqrs.Domain.Events.InventoryItemRenamed":
                    this.name = event.newName;
                    break;
                case "SimpleCqrs.Domain.Events.InventoryItemDeactivated":
                    this.activated = false;
                    break;
            }
        },

        rename: function (newName) {
            if (!newName || newName.length === 0) {
                throw "ArgumentException";
            }

            this.applyChange(new SimpleCqrs.Domain.Events.InventoryItemRenamed({ id: this.id, newName: newName }));
        },

        remove: function (count) {
            if (count <= 0) {
                throw "InvalidOperation";
            }

            this.applyChange(new SimpleCqrs.Domain.Events.ItemsRemovedFromInventory({ id: this.id, count: count }));
        },

        checkIn: function (count) {
            if (count <= 0) {
                throw "InvalidOperation";
            }

            this.applyChange(new SimpleCqrs.Domain.Events.ItemsCheckedInToInventory({ id: this.id, count: count }));
        },

        deactivate: function () {
            if (!this.activated) {
                throw "InvalidOperation";
            }

            this.applyChange(new SimpleCqrs.Domain.Events.InventoryItemDeactivated({ id: this.id }));
        }



    });
})