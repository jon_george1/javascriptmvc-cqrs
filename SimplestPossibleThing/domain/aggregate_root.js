steal('jquery/class', './events', function () {

    $.Class('SimpleCqrs.Domain.AggregateRoot',
    /* @Static */
        {

    },
    /* @Prototype */
        {
        changes: [],

        id: null,

        version: 0,

        getUncommittedChanges: function () {
            return this.changes;
        },

        markChangesAsCommitted: function () {
            this.changes = [];
        },

        loadFromHistory: function (events) {
            for (var i = 0; i < events.length; i++) {
                this.applyChange(events[i], false);
            }
        },

        apply: function (event) {
            throw "NotImplementedException";
        },

        applyChange: function (event, isNew) {
            if (isNew === undefined) {
                isNew = true;
            }

            this.apply(event);
            if (isNew) {
                event.setVersion(this.version + 1);
                this.changes.push(event);
            }

            this.version = event.version;
        }
    });

})