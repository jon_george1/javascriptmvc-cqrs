steal("framework/events").then(function () {
    SimpleCqrs.Framework.Events.EventFactory.createEvent("SimpleCqrs.Domain.Events.InventoryItemCreated", ["id", "name"]);
    SimpleCqrs.Framework.Events.EventFactory.createEvent("SimpleCqrs.Domain.Events.InventoryItemDeactivated", ["id"]);
    SimpleCqrs.Framework.Events.EventFactory.createEvent("SimpleCqrs.Domain.Events.InventoryItemRenamed", ["id", "newName"]);
    SimpleCqrs.Framework.Events.EventFactory.createEvent("SimpleCqrs.Domain.Events.ItemsCheckedInToInventory", ["id", "count"]);
    SimpleCqrs.Framework.Events.EventFactory.createEvent("SimpleCqrs.Domain.Events.ItemsRemovedFromInventory", ["id", "count"]);
});
