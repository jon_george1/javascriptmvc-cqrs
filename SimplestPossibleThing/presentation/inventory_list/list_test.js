steal('funcunit',function(){

module("presentation.InventoryListItems.List", { 
	setup: function(){
		S.open("//presentation/inventory_list_items/list/list.html");
	}
});

test("delete inventory_list_items", function(){
	S('#create').click()
	
	// wait until grilled cheese has been added
	S('h3:contains(Grilled Cheese X)').exists();
	
	S.confirm(true);
	S('h3:last a').click();
	
	
	S('h3:contains(Grilled Cheese)').missing(function(){
		ok(true,"Grilled Cheese Removed")
	});
	
});


});