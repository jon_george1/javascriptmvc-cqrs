steal('jquery/controller',
	   'jquery/view/ejs',
	   'jquery/controller/view',
	   'presentation/models',
       'framework/classes/guid.js',
       'domain')
.then('./views/init.ejs',
       './views/inventory_list_item.ejs',
       function ($) {

           /**
           * @class presentation.InventoryListItem.List
           * @parent index
           * @inherits jQuery.Controller
           * Lists inventory_list_item and lets you destroy them.
           */
           $.Controller('Presentation.InventoryList',
           /** @Static */
{
defaults: {}
},
           /** @Prototype */
{
init: function (el, repository, bus) {
    this.repository = repository;
    this.bus = bus;
    this.element.html(this.view('init', Presentation.Models.InventoryListItem.findAll()))
},
".destroy click": function (el) {
    if (confirm("Are you sure you want to destroy?")) {
        var model = el.closest('li').model();

        var command = new SimpleCqrs.ApplicationServices.Commands.DeactivateInventoryItem({ inventoryItemId: model.id });
        this.bus.send(command);
    }
},

".rename click": function (el) {
    var newName = prompt("Enter new name");
    if (newName) {
        var model = el.closest('li').model();
        var command = new SimpleCqrs.ApplicationServices.Commands.RenameInventoryItem({ inventoryItemId: model.id, newName: newName });
        this.bus.send(command);
    }
},

".checkIn click": function (el) {
    var quantity = prompt("Enter quantity to check in");
    if (quantity) {
        var model = el.closest('li').model();
        var command = new SimpleCqrs.ApplicationServices.Commands.CheckInInventory({ inventoryItemId: model.id, quantity: quantity });
        this.bus.send(command);
    }
},

".remove click": function (el) {
    var quantity = prompt("Enter quantity to remove");
    if (quantity) {
        var model = el.closest('li').model();
        var command = new SimpleCqrs.ApplicationServices.Commands.RemoveInventory({ inventoryItemId: model.id, quantity: quantity });
        this.bus.send(command);
    }
},

"{Presentation.Models.InventoryListItem} destroyed": function (InventoryListItem, ev, inventory_list_item) {
    inventory_list_item.elements(this.element).remove();
},

"{Presentation.Models.InventoryListItem} created": function (InventoryListItem, ev, inventory_list_item) {
    $("ul", this.element).append(this.view('inventory_list_item', inventory_list_item));
},

"{Presentation.Models.InventoryListItem} updated": function (InventoryListItem, ev, inventory_list_item) {
    inventory_list_item.elements(this.element).html(this.view('inventory_list_item', inventory_list_item));
},

"#addInventoryItemForm submit": function (elt, evt) {
    evt.preventDefault();

    var command = new SimpleCqrs.ApplicationServices.Commands.CreateInventoryItem({ name: $("#newName").val() });
    this.bus.send(command);

    $("#addInventoryItemForm")[0].reset();
}

});

       });