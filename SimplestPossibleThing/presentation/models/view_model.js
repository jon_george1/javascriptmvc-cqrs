steal('jquery/model', 'framework/plugins/jquery.indexeddb.js', function () {

    /**
    * @class Presentation.Models.InventoryListItems
    * @parent index
    * @inherits jQuery.Model
    * Wraps backend inventory_list_items services.  
    */
    $.Model('Presentation.Models.ViewModel',
    /* @Static */
    {
    storage: [],
    databaseName: "readModelStore5",
    tableName: "readModels",
    modelTypeIndexName: "modelType",

    createDeferred: function (success, error) {
        var result = $.Deferred();
        if (success) result.then(success);
        if (error) result.fail(error);
        return result;
    },

    findAll: function (params) {
        var result = $.Deferred();
        
        var data = [];

        var request = $.indexedDB(this.databaseName).objectStore(this.tableName).index(this.modelTypeIndexName).each(this.callback(function(el) {
            if (el.key === this.fullName) {
                data.push(this.model(el.value));
            }
            return true;
        })).then(function() {
            result.resolve(data);
        }, function (err) {
            steal.dev.log(err);
        });

        return result;
    },

    findOne: function (params, success, error) {
        var result = $.Deferred();

        $.indexedDB(this.databaseName).objectStore(this.tableName).get(params).then(this.callback(function (data) {
            var model = this.model(data);
            result.resolve(model);
        }), error);

        return result;
    },

    create: function (attrs, success, error) {
        attrs.id = attrs.aggregateId;
        delete attrs.aggregateId;

        $.indexedDB(this.databaseName).objectStore(this.tableName).add(attrs).then(function() {
            success({ id: attrs.id });
        }, error);
    },

    update: function (id, attrs, success, error) {
        return $.indexedDB(this.databaseName).objectStore(this.tableName).put(attrs).then(success, error);
    },

    destroy: function (id, success, error) {
        return $.indexedDB(this.databaseName).objectStore(this.tableName).delete(id).then(success, error);
    }
},

    {
        setup: function (attrs) {
            this.attrs(attrs);
            this.attr("modelType", this.Class.fullName);
        }
    });

}).then(function () {
    // Create the readmodel db
    return $.indexedDB(Presentation.Models.ViewModel.databaseName, {
        "version": 1,
        "upgrade": function (transaction) { },
        "schema": {
            "1": function (transaction) { 
                var objectStore = transaction.createObjectStore(Presentation.Models.ViewModel.tableName, { keyPath: "id", autoIncrement: false });
                objectStore.createIndex("modelType", { unique: false }, Presentation.Models.ViewModel.modelTypeIndexName);
            },
        }
    });

});