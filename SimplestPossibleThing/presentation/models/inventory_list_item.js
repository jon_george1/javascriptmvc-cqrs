steal(
    'jquery/model',
    './view_model',
    function () {

        /**
        * @class Presentation.Models.InventoryListItems
        * @parent index
        * @inherits jQuery.Model
        * Wraps backend inventory_list_items services.  
        */
        Presentation.Models.ViewModel('Presentation.Models.InventoryListItem',
        /* @Static */
    {
    },

    {
    });

    })