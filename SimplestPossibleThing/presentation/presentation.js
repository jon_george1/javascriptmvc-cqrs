steal('./presentation.css',
	   'steal/coffee',
        'domain',
        'application_services',
        'infrastructure',
        './denormalizers/denormalizers',
        './models/models',
        './inventory_list/list')
.then(function () {

    // Create event storage and wire up
    var bus = new SimpleCqrs.Infrastructure.Bus();
    var eventStore = new SimpleCqrs.Infrastructure.EventStore(bus);
    var repository = new SimpleCqrs.Infrastructure.Repository(eventStore);

    // Create denormalizers
    Presentation.Denormalizers.Installer.install(bus);

    // Create command handlers
    SimpleCqrs.ApplicationServices.Installer.install(bus, repository);

    $("#content").presentation_inventory_list(repository, bus);

});