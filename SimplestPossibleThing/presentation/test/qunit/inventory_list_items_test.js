steal("funcunit/qunit", "presentation/fixtures", "presentation/models/inventory_list_items.js", function(){
	module("Model: Presentation.Models.InventoryListItems")
	
	test("findAll", function(){
		expect(4);
		stop();
		Presentation.Models.InventoryListItems.findAll({}, function(inventory_list_items){
			ok(inventory_list_items)
	        ok(inventory_list_items.length)
	        ok(inventory_list_items[0].name)
	        ok(inventory_list_items[0].description)
			start();
		});
		
	})
	
	test("create", function(){
		expect(3)
		stop();
		new Presentation.Models.InventoryListItems({name: "dry cleaning", description: "take to street corner"}).save(function(inventory_list_items){
			ok(inventory_list_items);
	        ok(inventory_list_items.id);
	        equals(inventory_list_items.name,"dry cleaning")
	        inventory_list_items.destroy()
			start();
		})
	})
	test("update" , function(){
		expect(2);
		stop();
		new Presentation.Models.InventoryListItems({name: "cook dinner", description: "chicken"}).
	            save(function(inventory_list_items){
	            	equals(inventory_list_items.description,"chicken");
	        		inventory_list_items.update({description: "steak"},function(inventory_list_items){
	        			equals(inventory_list_items.description,"steak");
	        			inventory_list_items.destroy();
						start();
	        		})
	            })
	
	});
	test("destroy", function(){
		expect(1);
		stop();
		new Presentation.Models.InventoryListItems({name: "mow grass", description: "use riding mower"}).
	            destroy(function(inventory_list_items){
	            	ok( true ,"Destroy called" )
					start();
	            })
	})
})