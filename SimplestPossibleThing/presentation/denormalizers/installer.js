steal(
    "jquery/class", 
    function () {

    $.Class('Presentation.Denormalizers.Installer',
    /* @Static */
        {
        install: function (bus) {
            this.inventoryListItemDenormalizer = new Presentation.Denormalizers.InventoryListItem(bus);
        }
    },
    /* @Prototype */
        {
    });

})