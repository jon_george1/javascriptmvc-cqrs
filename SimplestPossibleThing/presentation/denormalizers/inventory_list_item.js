steal('jquery/class', function () {

    $.Class('Presentation.Denormalizers.InventoryListItem',
        /* @Static */
        {

        },
        /* @Prototype */
        {
            init: function (eventPublisher) {
                eventPublisher.registerHandler(SimpleCqrs.Domain.Events.InventoryItemCreated.fullName, this.callback(this.handleInventoryItemCreated));
                eventPublisher.registerHandler(SimpleCqrs.Domain.Events.InventoryItemRenamed.fullName, this.callback(this.handleInventoryItemRenamed));
                eventPublisher.registerHandler(SimpleCqrs.Domain.Events.InventoryItemDeactivated.fullName, this.callback(this.handleInventoryItemDeactivated));
            },

            handleInventoryItemCreated: function (event) {
                var model = new Presentation.Models.InventoryListItem({ aggregateId: event.id, name: event.name });
                model.save();
            },

            handleInventoryItemRenamed: function (event) {
                Presentation.Models.InventoryListItem.findOne(event.id).then(function (model) {
                    model.attr("name", event.newName);
                    model.save();
                });
            },

            handleInventoryItemDeactivated: function (event) {
                Presentation.Models.InventoryListItem.findOne(event.id).then(function (model) {
                    model.destroy();
                });
            }

        });

})