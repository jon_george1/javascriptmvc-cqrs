steal('jquery/class', function () {

    $.Class('SimpleCqrs.Infrastructure.Database',
    /* @Static */
        {

    },
    /* @Prototype */
        {
        init: function (databaseName, createMethod) {
            this.indexedDb = window.indexedDB || window.webkitIndexedDB || window.mozIndexedDB || window.msIndexedDB;

            if (this.indexedDb === undefined) {
                throw "StorageUnavailable";
            }

            this.IDBTransaction = window.IDBTransaction || window.webkitIDBTransaction;
            this.databaseName = databaseName;
            this.createMethod = createMethod;
        },

        connect: function() {
            var result = $.Deferred();

            if (this.db == undefined) {
                var version = 7;
                var request = this.indexedDb.open(this.databaseName, version);

                request.onsuccess = this.callback(function (evt) { 
                    this.db = evt.currentTarget.result; 

                    if (this.db.version < version && this.db.setVersion) {
                        var request = this.db.setVersion(version);
                        request.onsuccess = this.callback(function (evt) {
                            this.createMethod(evt.currentTarget.result.db);
                            result.resolve(this);
                        });
                    } else {
                        result.resolve(this);
                    }

                });
                request.onerror = this.callback(function (evt) { 
                    steal.dev.log("IndexedDb fail"); 
                });
                request.onupgradeneeded = function (evt) {
                    this.createMethod(evt.currentTarget.result);
                };
            } else {
                result.resolve(this);
            }
            
            return result;
        },

        save: function (document, table) {
            var transaction = this.db.transaction(table, "readwrite");
            var store = transaction.objectStore(table);
            var result = $.Deferred();

            var request = store.put(document);
            request.onsuccess = function (evt) { result.resolve(); };
            request.onerror = function (evt) { result.reject(evt); };
            
            return result;
        },

        remove: function (documentId, table) {
            var transaction = this.db.transaction(table, "readwrite");
            var store = transaction.objectStore(table);
            var result = $.Deferred();

            var request = store.delete(documentId);
            request.onsuccess = function (evt) { result.resolve(); };
            request.onerror = function (evt) { result.reject(evt); };
            
            return result;
        },

        getById: function(documentId, table) {
            var transaction = this.db.transaction(table, "readonly");
            var store = transaction.objectStore(table);
            var result = $.Deferred();

            var request = store.get(documentId);
            request.onsuccess = function (evt) { result.resolve(request.result); };
            request.onerror = function (evt) { result.reject(evt); };
            
            return result;
        },

        getAll: function(table) {
            var transaction = this.db.transaction(table, "readonly");
            var store = transaction.objectStore(table);
            var result = $.Deferred();

            var request = store.openCursor();
            var data = [];
            request.onsuccess = function (evt) { 
                var cursor = evt.target.result;
                if (cursor) {
                    data.push(cursor.value);
                    cursor.continue();
                } else {
                    result.resolve(data); 
                }
            };
            request.onerror = function (evt) { result.reject(evt); };
            
            return result;
        }, 

        findOne: function(val, index, table) {
            var transaction = this.db.transaction(table, "readonly");
            var store = transaction.objectStore(table);
            var index = store.index(index);
            var result = $.Deferred();

            var request = index.get(val);
            request.onsuccess = function (evt) { result.resolve(request.result); };
            request.onerror = function (evt) { result.reject(evt); };
            
            return result;
        },

        findAll: function(val, index, table) {
            var transaction = this.db.transaction(table, "readonly");
            var store = transaction.objectStore(table);
            var index = store.index(index);
            var result = $.Deferred();

            var request = index.openCursor(val);
            var data = [];
            request.onsuccess = function (evt) { 
                var cursor = evt.target.result;
                if (cursor) {
                    data.push(cursor.value);
                    cursor.continue();
                } else {
                    result.resolve(data); 
                }
            };
            request.onerror = function (evt) { result.reject(evt); };
            
            return result;
        }

    });

})