steal('jquery/class', function () {

    $.Class('SimpleCqrs.Infrastructure.Repository',
    /* @Static */
        {

    },
    /* @Prototype */
        {
        init: function (eventStore) {
            this.eventStore = eventStore;
        },

        save: function (aggregate) {
            this.eventStore.saveEvents(aggregate.id, aggregate.getUncommittedChanges());
            aggregate.markChangesAsCommitted();
        },

        getById: function (Type, aggregateId) {
            var result = $.Deferred();

            this.eventStore.getEventsForAggregate(aggregateId).then(function (events) {
                var obj = new Type(); // This won't work
                obj.loadFromHistory(events);

                result.resolve(obj);
            });

            return result;
        }
    });

})