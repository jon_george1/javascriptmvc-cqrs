steal('jquery/class', 'framework/plugins/jquery.indexeddb.js', function () {

    $.Class('SimpleCqrs.Infrastructure.EventStore',
    /* @Static */
        {
        databaseName: "eventStore2",
        tableName: "events",
        aggregateIdIndexName: "aggregateId"
    },
    /* @Prototype */
        {
        databaseName: "eventStore2",
        tableName: "events",
        aggregateIdIndexName: "aggregateId",

        init: function (publisher) {
            this.publisher = publisher;
        },

        saveEvents: function (aggregateId, events) {
            if (events && events.length > 0) {

                this.getEventsForAggregate(aggregateId).then(this.callback(function (eventStream) {
                    var expectedVersion = events[0].version - 1;
                    var actualVersion = eventStream.length === 0 ? 0 : eventStream[eventStream.length - 1].version;

                    if (expectedVersion !== actualVersion) {
                        throw "ConcurrencyException";
                    }

                    for (var i = 0; i < events.length; i++) {
                        var current = events[i];
                        $.indexedDB(this.databaseName).objectStore(this.tableName).add(current).then(this.callback(function () {
                            this.publisher.publish(current);
                        }));
                    }
                }));
            }
        },

        getEventsForAggregate: function (aggregateId) {
            var result = $.Deferred();

            var data = [];

            $.indexedDB(this.databaseName).objectStore(this.tableName).index(this.aggregateIdIndexName).each(function(el) {
                if (el.key === aggregateId) {
                    data.push(el.value);
                }

                return true;
            }).then(function() {
                result.resolve(data);
            });

            return result;
        }
    });

}).then(function () {
    // Create the readmodel db
    return $.indexedDB(SimpleCqrs.Infrastructure.EventStore.databaseName, {
        "version": 1,
        "upgrade": function (transaction) { },
        "schema": {
            "1": function (transaction) { 
                var objectStore = transaction.createObjectStore(SimpleCqrs.Infrastructure.EventStore.tableName, { keyPath: "eventId", autoIncrement: false });
                objectStore.createIndex("id", { unique: false }, SimpleCqrs.Infrastructure.EventStore.aggregateIdIndexName);
            },
        }
    });

});