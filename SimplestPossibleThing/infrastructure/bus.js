steal('jquery/class', function () {

    $.Class('SimpleCqrs.Infrastructure.Bus',
        /* @Static */
        {

        },
        /* @Prototype */
        {
            routes: {},

            registerHandler: function (fullTypeName, handler) {
                if (this.routes[fullTypeName] === undefined) {
                    this.routes[fullTypeName] = [];
                }

                this.routes[fullTypeName].push(handler);
            },
            
            send: function (command) {
                var handlers = this.routes[command.Class.fullName];
                if (handlers === undefined || handlers.length !== 1) {
                    throw "InvalidOperationException";
                }
                
                handlers[0](command);
            },
            
            publish: function (event) {
                var handlers = this.routes[event.eventName];
                if (handlers === undefined) {
                    return;
                }

                for (var i = 0; i < handlers.length; i++) {
                    handlers[i](event);
                }
            }
        });

})