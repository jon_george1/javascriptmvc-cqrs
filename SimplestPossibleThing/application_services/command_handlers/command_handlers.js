steal(
    "./create_inventory_item",
    "./check_in_inventory",
    "./deactivate_inventory_item",
    "./remove_inventory",
    "./rename_inventory_item"
);
