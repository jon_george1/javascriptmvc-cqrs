steal("jquery/class", "domain", "framework/commands", function () {

    $.Class('SimpleCqrs.ApplicationServices.CommandHandlers.RemoveInventory',
    /* @Static */
        {

    },
    /* @Prototype */
        {
        init: function (commandSender, repository) {
            SimpleCqrs.Framework.Commands.CommandFactory.createCommand("SimpleCqrs.ApplicationServices.Commands.RemoveInventory", ["inventoryItemId", "quantity"]);
            commandSender.registerHandler(SimpleCqrs.ApplicationServices.Commands.RemoveInventory.fullName, this.callback(this.handle));
            this.repository = repository;
        },

        handle: function (command) {
            this.repository.getById(SimpleCqrs.Domain.InventoryItem, command.inventoryItemId).then(this.callback(function (item) {
                item.remove(command.quantity);
                this.repository.save(item);
            }));
        }
    });

})