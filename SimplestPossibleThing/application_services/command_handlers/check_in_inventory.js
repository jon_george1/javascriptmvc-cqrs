steal("jquery/class", "domain", "infrastructure", "framework/commands", function () {

    $.Class('SimpleCqrs.ApplicationServices.CommandHandlers.CheckInInventory',
    /* @Static */
        {

    },
    /* @Prototype */
        {
        init: function (commandSender, repository) {
            SimpleCqrs.Framework.Commands.CommandFactory.createCommand("SimpleCqrs.ApplicationServices.Commands.CheckInInventory", ["inventoryItemId", "quantity"]);
            commandSender.registerHandler(SimpleCqrs.ApplicationServices.Commands.CheckInInventory.fullName, this.callback(this.handle));
            this.repository = repository;
        },

        handle: function (command) {
            this.repository.getById(SimpleCqrs.Domain.InventoryItem, command.inventoryItemId).then(this.callback(function (item) {
                item.checkIn(command.quantity);
                this.repository.save(item);
            }));
        }
    });

})