steal("jquery/class", "domain", "framework/commands", function () {

    $.Class('SimpleCqrs.ApplicationServices.CommandHandlers.CreateInventoryItem',
    /* @Static */
        {

    },
    /* @Prototype */
        {
        init: function (commandSender, repository) {
            SimpleCqrs.Framework.Commands.CommandFactory.createCommand("SimpleCqrs.ApplicationServices.Commands.CreateInventoryItem", ["name"]);
            commandSender.registerHandler(SimpleCqrs.ApplicationServices.Commands.CreateInventoryItem.fullName, this.callback(this.handle));
            this.repository = repository;
        },

        handle: function (command) {
            var newItem = new SimpleCqrs.Domain.InventoryItem(SimpleCqrs.Framework.Guid.create(), command.name);
            this.repository.save(newItem);
        }
    });

})