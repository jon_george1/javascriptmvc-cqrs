steal("jquery/class", "domain", "framework/commands", function () {

    $.Class('SimpleCqrs.ApplicationServices.CommandHandlers.DeactivateInventoryItem',
    /* @Static */
        {

    },
    /* @Prototype */
        {
        init: function (commandSender, repository) {
            SimpleCqrs.Framework.Commands.CommandFactory.createCommand("SimpleCqrs.ApplicationServices.Commands.DeactivateInventoryItem", ["inventoryItemId"]);
            commandSender.registerHandler(SimpleCqrs.ApplicationServices.Commands.DeactivateInventoryItem.fullName, this.callback(this.handle));
            this.repository = repository;
        },

        handle: function (command) {
            this.repository.getById(SimpleCqrs.Domain.InventoryItem, command.inventoryItemId).then(this.callback(function (item) {
                item.deactivate();
                this.repository.save(item);
            }));
        }
    });

})