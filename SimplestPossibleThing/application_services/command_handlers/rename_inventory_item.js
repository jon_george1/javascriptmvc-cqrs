steal("jquery/class", "domain", "framework/commands", function () {

    $.Class('SimpleCqrs.ApplicationServices.CommandHandlers.RenameInventoryItem',
    /* @Static */
        {

    },
    /* @Prototype */
        {
        init: function (commandSender, repository) {
            SimpleCqrs.Framework.Commands.CommandFactory.createCommand("SimpleCqrs.ApplicationServices.Commands.RenameInventoryItem", ["inventoryItemId", "newName"]);
            commandSender.registerHandler(SimpleCqrs.ApplicationServices.Commands.RenameInventoryItem.fullName, this.callback(this.handle));
            this.repository = repository;
        },

        handle: function (command) {
            this.repository.getById(SimpleCqrs.Domain.InventoryItem, command.inventoryItemId).then(this.callback(function (item) {
                item.rename(command.newName);
                this.repository.save(item);
            }));
        }
    });

})