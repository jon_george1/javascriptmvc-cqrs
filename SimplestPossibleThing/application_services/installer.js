steal(
    "./command_handlers/command_handlers", 
    function () {

    $.Class('SimpleCqrs.ApplicationServices.Installer',
    /* @Static */
        {
        install: function (bus, repository) {
            this.createInventoryItemHandler = new SimpleCqrs.ApplicationServices.CommandHandlers.CreateInventoryItem(bus, repository);
            this.deactivateInventoryItemHandler = new SimpleCqrs.ApplicationServices.CommandHandlers.DeactivateInventoryItem(bus, repository);
            this.renameInventoryItemHandler = new SimpleCqrs.ApplicationServices.CommandHandlers.RenameInventoryItem(bus, repository);
            this.check_in_inventory = new SimpleCqrs.ApplicationServices.CommandHandlers.CheckInInventory(bus, repository);
            this.remove_inventory = new SimpleCqrs.ApplicationServices.CommandHandlers.RemoveInventory(bus, repository);
        }
    },
    /* @Prototype */
        {
    });

})